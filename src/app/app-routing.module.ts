import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddRecipeComponent } from './recipes/add-recipe/add-recipe.component';
import { RecipesComponent } from './recipes/recipes.component';
import { RecipeDetailsComponent } from './recipes/recipe-details/recipe-details.component';
import { RecipeResolverService } from './recipes/recipe-resolver.service';

const routes: Routes = [
  {path: '', component: RecipesComponent},
  {path: 'add', component: AddRecipeComponent},
  {path: 'recipes/:id', component: RecipeDetailsComponent, resolve: {
    recipe: RecipeResolverService
    }},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
