import { Component, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { RecipeService } from '../../shared/recipe.service';
import { Recipe } from '../../shared/recipe.model';

@Component({
  selector: 'app-edit-recipe',
  templateUrl: './add-recipe.component.html',
  styleUrls: ['./add-recipe.component.css']
})
export class AddRecipeComponent implements OnInit {
  recipeForm!: FormGroup;
  isClick:boolean = true;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private recipeService: RecipeService
  ) { }

  ngOnInit(): void {
    this.recipeForm = new FormGroup({
      name: new FormControl('', Validators.required),
      description: new FormControl('', Validators.required),
      imageRecipe: new FormControl('', Validators.required),
      ingredients: new FormControl('', Validators.required),
      steps: new FormArray([]),
    });
  }

  addStep() {
    const steps = <FormArray>this.recipeForm.get('steps');
    const stepGroup = new FormGroup({
      imageStep: new FormControl('', Validators.required),
      descriptionStep: new FormControl('', Validators.required)
    });
    steps.push(stepGroup);
    this.isClick = false;
  }

  getSkillControls() {
    const steps = <FormArray>this.recipeForm.get('steps');
    return steps.controls;
  }

  fieldHasError(fieldName: string, errorType: string) {
    const field = this.recipeForm.get(fieldName);
    return Boolean(field && field.touched && field.errors?.[errorType]);
  }

  saveRecipe() {
    const id = Math.random().toString();
    const recipe = new Recipe(
      id,
      this.recipeForm.value.description,
      this.recipeForm.value.imageRecipe,
      this.recipeForm.value.ingredients,
      this.recipeForm.value.name,
      this.recipeForm.value.steps,
    );

    this.recipeService.addRecipe(recipe).subscribe(() => {
      this.recipeService.fetchRecipes();
        void this.router.navigate(['..'], {relativeTo: this.route});
    });
  }
}
