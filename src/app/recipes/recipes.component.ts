import { Component, OnDestroy, OnInit } from '@angular/core';
import { Recipe } from '../shared/recipe.model';
import { Subscription } from 'rxjs';
import { RecipeService } from '../shared/recipe.service';

@Component({
  selector: 'app-recipes',
  templateUrl: './recipes.component.html',
  styleUrls: ['./recipes.component.css']
})
export class RecipesComponent implements OnInit, OnDestroy {
  recipes!: Recipe[];
  recipesChangeSubscription!: Subscription;

  constructor(private recipeService: RecipeService) { }

  ngOnInit(): void {
    this.recipesChangeSubscription = this.recipeService.recipesFetch.subscribe((recipes: Recipe[]) => {
      this.recipes = recipes;
    });
    this.recipeService.fetchRecipes();
  }

  ngOnDestroy(): void {
    this.recipesChangeSubscription.unsubscribe();
  }

}
