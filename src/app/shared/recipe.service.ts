import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Recipe } from './recipe.model';
import { map, Subject } from 'rxjs';

@Injectable()

export class RecipeService {
  recipesFetch = new Subject<Recipe[]>();

  constructor(private http: HttpClient) {}

  private recipes: Recipe[] = [];

  addRecipe(recipe: Recipe) {
    const body = {
      description: recipe.description,
      imageRecipe: recipe.imageRecipe,
      ingredients: recipe.ingredients,
      name: recipe.name,
      steps: recipe.steps,
    };

    return this.http.post('https://plovo-cc061-default-rtdb.firebaseio.com/recipes.json', body);
  }

  fetchRecipe(id: string) {
    return this.http.get<Recipe>(`https://plovo-cc061-default-rtdb.firebaseio.com/recipes/${id}.json`).pipe(
      map(result => {
        return new Recipe(result.id, result.description ,result.imageRecipe, result.ingredients, result.name, result.steps);
      })
    )
  }

  fetchRecipes() {
    this.http.get<{[id: string]: Recipe}>('https://plovo-cc061-default-rtdb.firebaseio.com/recipes.json').pipe(
      map(result => {
        return Object.keys(result).map(id => {
          const recipeData = result[id];

          return new Recipe(
            id,
            recipeData.description,
            recipeData.imageRecipe,
            recipeData.ingredients,
            recipeData.name,
            recipeData.steps,
          );
        });
      })
    ).subscribe(recipes => {
      this.recipes = recipes;
      this.recipesFetch.next(this.recipes.slice());
    })
  }

  removeRecipe(id: string) {
    return this.http.delete(`https://plovo-cc061-default-rtdb.firebaseio.com/recipes/${id}.json`);
  }
}
