export class Recipe {
  constructor(
    public id: string,
    public description: string,
    public imageRecipe: string,
    public ingredients: string,
    public name: string,
    public steps: any,
  ) {}
}
